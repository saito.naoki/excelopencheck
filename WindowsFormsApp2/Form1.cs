﻿using System;
using System.Windows.Forms;
using System.Linq;

// Office プライマリ相互運用機能アセンブリ(Office Excel Primary Interop Assembly)
// 参照設定で「Microsoft Excel 15.0 Object Library」を追加しておく
// https://docs.microsoft.com/ja-jp/visualstudio/vsto/office-primary-interop-assemblies?view=vs-2017
// http://msdn.microsoft.com/ja-jp/library/ms262200(v=office.11).aspx
using Excel = Microsoft.Office.Interop.Excel;

// ClosedXML
// NuGetパッケージマネージャからClosedXMLをインストールしておく
// https://github.com/closedxml/closedxml/wiki
using ClosedXML.Excel;

// OpenXML
// NuGetパッケージマネージャからOpenXMLをインストールしておく
// https://docs.microsoft.com/ja-jp/office/open-xml/open-xml-sdk
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;



namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        /*
        * Excel 制御
        */
        /// <summary>
        /// Excelアプリケーション
        /// </summary>
        private String fileName = null;
        /// <summary>
        /// Excelアプリケーション
        /// </summary>
        private Excel.Application oXL = null;
        /// <summary>
        /// ワークブック
        /// </summary>
        private Excel.Workbook oWB = null;
        /// <summary>
        /// 

        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                // Excelアプリオブジェクトの生成と開始
                if (oXL == null)
                {
                    oXL = new Excel.Application();
                }

                // ブックおよびシートの取得
                oWB = oXL.Workbooks.Open(fileName);


                // Excel 画面の表示
                oXL.Visible = true;
                oXL.UserControl = true; // これがよくわからない
            }
            catch (Exception theException)
            {
                String errorMessage;
                errorMessage = "Error: ";
                errorMessage = String.Concat(errorMessage, theException.Message);
                errorMessage = String.Concat(errorMessage, " Line: ");
                errorMessage = String.Concat(errorMessage, theException.Source);

                MessageBox.Show(errorMessage, "Error");
            }
            finally
            {
            }

        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Excelアプリオブジェクトが存在する場合，閉じる
            if (oXL != null)
            {
                try
                {
                    oXL.Quit();
                }
                finally
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oXL);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                using (var cWB = new XLWorkbook(fileName))
                {
                    var worksheet = cWB.Worksheets.Worksheet(1);
                    var cella1 = worksheet.Cell("A1").Value;
                    MessageBox.Show("A1 = " + cella1.ToString());
                }
            }
            catch (Exception theException)
            {
                String errorMessage;
                errorMessage = "Error: ";
                errorMessage = String.Concat(errorMessage, theException.Message);
                errorMessage = String.Concat(errorMessage, " Line: ");
                errorMessage = String.Concat(errorMessage, theException.Source);

                MessageBox.Show(errorMessage, "Error");
            }
            finally
            {
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                using (var document = SpreadsheetDocument.Open(fileName, false))
                {
                    var wbPart = document.WorkbookPart;
                    var stringTable = wbPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

                    foreach (var sheet in wbPart.Workbook.Descendants<Sheet>())
                    {
                        if (sheet.Name == "Sheet1")
                        {
                            var wsheetPart = wbPart.GetPartById(sheet.Id) as WorksheetPart;
                            if (wsheetPart == null)
                            {
                                MessageBox.Show("WorksheetPart Not Found !!");
                                return;
                            }

                            var ws = wsheetPart.Worksheet;
                            var row1 = ws.Descendants<Row>().ElementAt(0);
                            var colA = row1.Descendants<Cell>().ElementAt(0);

                            string cellVal = colA.InnerText;
                            if (colA.DataType != null)
                            {
                                switch (colA.DataType.Value)
                                {
                                    case CellValues.Boolean:
                                    case CellValues.Date:
                                    case CellValues.Error:
                                    case CellValues.InlineString:
                                    case CellValues.Number:
                                    case CellValues.String:
                                        cellVal = colA.InnerText;
                                        break;
                                    case CellValues.SharedString:
                                        if (stringTable != null)
                                        {
                                            cellVal = stringTable.SharedStringTable.ElementAt(int.Parse(cellVal)).InnerText;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }

                            MessageBox.Show("A1 = " + cellVal);
                        }
                    }
                }
            }
            catch (Exception theException)
            {
                String errorMessage;
                errorMessage = "Error: ";
                errorMessage = String.Concat(errorMessage, theException.Message);
                errorMessage = String.Concat(errorMessage, " Line: ");
                errorMessage = String.Concat(errorMessage, theException.Source);

                MessageBox.Show(errorMessage, "Error");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();

            openDialog.Title = "ファイルを選ぶ";
            openDialog.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            openDialog.Filter = "すべてのファイル|*.*";

            // ファイルパスを取得したら，開く
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                fileName = openDialog.FileName;
                Console.WriteLine(fileName);
                label2.Text = fileName;
            }

        }
    }
}
